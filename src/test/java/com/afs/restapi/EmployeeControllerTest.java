package com.afs.restapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@AutoConfigureMockMvc
@SpringBootTest
class EmployeeControllerTest {

    @Autowired
    private MockMvc client;

    @Autowired
    private EmployeeRepository employeeRepository;

    private ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    public void clearEmployeeRepository(){
        employeeRepository.clearAll();
    }

    @Test
    void should_return_employees_when_perform_get_given_employees_in_repo() throws Exception {
        Employee employeeCharlie = getEmployeeCharlie();
        Employee employeeJaden = getEmployeeJaden();
        employeeRepository.insert(employeeCharlie);
        employeeRepository.insert(employeeJaden);

        client.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("Charlie"))
                .andExpect(jsonPath("$[0].age").isNumber())
                .andExpect(jsonPath("$[0].age").value("30"))
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].name").isString())
                .andExpect(jsonPath("$[1].name").value("Jaden"))
                .andExpect(jsonPath("$[1].age").isNumber())
                .andExpect(jsonPath("$[1].age").value("30"));
    }

    @Test
    void should_return_employee_match_id_when_perform_get_given_employees_and_id() throws Exception {
        Employee employeeCharlie = getEmployeeCharlie();
        Employee employeeJaden = getEmployeeJaden();
        employeeRepository.insert(employeeCharlie);
        employeeRepository.insert(employeeJaden);

        client.perform(MockMvcRequestBuilders.get("/employees/{id}", 1))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").isString())
                .andExpect(jsonPath("$.name").value("Charlie"))
                .andExpect(jsonPath("$.age").isNumber())
                .andExpect(jsonPath("$.age").value("30"));
    }



    @Test
    void should_return_employees_match_gender_when_perform_get_given_employees_and_gender() throws Exception {
        Employee employeeCharlie = getEmployeeCharlie();
        Employee employeeJaden = getEmployeeJaden();
        employeeRepository.insert(employeeCharlie);
        employeeRepository.insert(employeeJaden);

        client.perform(MockMvcRequestBuilders.get("/employees").param("gender", "male"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("Charlie"))
                .andExpect(jsonPath("$[0].age").isNumber())
                .andExpect(jsonPath("$[0].age").value("30"))
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].name").isString())
                .andExpect(jsonPath("$[1].name").value("Jaden"))
                .andExpect(jsonPath("$[1].age").isNumber())
                .andExpect(jsonPath("$[1].age").value("30"));
    }

    @Test
    void should_return_employees_page_when_perform_get_given_employees_and_page_message() throws Exception {
        Employee employeeCharlie = getEmployeeCharlie();
        Employee employeeJaden = getEmployeeJaden();
        employeeRepository.insert(employeeCharlie);
        employeeRepository.insert(employeeJaden);

        client.perform(MockMvcRequestBuilders.get("/employees").param("page", "1").param("size", "2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("Charlie"))
                .andExpect(jsonPath("$[0].age").isNumber())
                .andExpect(jsonPath("$[0].age").value("30"))
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].name").isString())
                .andExpect(jsonPath("$[1].name").value("Jaden"))
                .andExpect(jsonPath("$[1].age").isNumber())
                .andExpect(jsonPath("$[1].age").value("30"));
    }

    @Test
    void should_return_employee_and_repository_save_it_when_perform_post_given_employee() throws Exception {
        Employee employeeCharlie = getEmployeeCharlie();
        String charlieJson = mapper.writeValueAsString(employeeCharlie);

        client.perform(MockMvcRequestBuilders.post("/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .content(charlieJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").isString())
                .andExpect(jsonPath("$.name").value("Charlie"))
                .andExpect(jsonPath("$.age").isNumber())
                .andExpect(jsonPath("$.age").value("30"));

        Employee dbEmployee = employeeRepository.findById(1L);
        assertEquals(employeeCharlie.getId(), dbEmployee.getId());

    }
    private Employee getEmployeeCharlie() {
        return new Employee(1L, "Charlie", 30, "male", 100000);
    }
    private Employee getEmployeeJaden() {
        return new Employee(2L, "Jaden", 30, "male", 100000);
    }
}
